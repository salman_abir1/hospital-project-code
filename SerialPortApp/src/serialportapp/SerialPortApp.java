/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serialportapp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 *
 * @author DIU
 */
public class SerialPortApp {

    /**
     * @param args the command line arguments
     */
    
    public static SerialPort serialPort;
    
    public static void main(String[] args) {
        // TODO code application logic here
        String s = "7";
        byte[] bytes; 
        try {
            bytes = s.getBytes("US-ASCII");
            System.out.println(Arrays.toString(bytes));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SerialPortApp.class.getName()).log(Level.SEVERE, null, ex);
        }
                    
        
                String[] portNames = SerialPortList.getPortNames();
        
        if (portNames.length == 0) {
            System.out.println("There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
            System.out.println("Press Enter to exit...");
            try {
                System.in.read();
            } catch (IOException e) {
                 // TODO Auto-generated catch block
                  e.printStackTrace();
            }
            return;
        }

        for (int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }
        
        
        serialPort = new SerialPort("COM23");
        try {
            serialPort.openPort();

            serialPort.setParams(SerialPort.BAUDRATE_9600,
                                 SerialPort.DATABITS_8,
                                 SerialPort.STOPBITS_1,
                                 SerialPort.PARITY_NONE);

            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN);

            serialPort.addEventListener(new PortReader());

            System.out.println("Port Opened");
            //serialPort.writeString("Hurrah!");
        }
        catch (SerialPortException ex) {
            System.out.println("There are an error on writing string to port т: " + ex);
        }

    }
    
    private static class PortReader implements SerialPortEventListener {
        String data = "";
            
        @Override
        public void serialEvent(SerialPortEvent event) {
            System.out.println("Data Received");
            
            if(event.isRXCHAR() && event.getEventValue() > 0) {
                try {
                    String receivedData = serialPort.readString(event.getEventValue());
                    //System.out.print(receivedData);
                    
                    data = data + receivedData;
                    
                    //byte[] bytes = receivedData.getBytes("US-ASCII"); 
                    //System.out.println(Arrays.toString(bytes));
              }
                catch (Exception ex) {
                    System.out.println("Error in receiving string from COM-port: " + ex);
                }
            }
            
            
                    byte[] bytes; 
            try {
                System.out.println(data);
                bytes = data.getBytes("US-ASCII");
                System.out.println(Arrays.toString(bytes));
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(SerialPortApp.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
            
        }

    }    
}
